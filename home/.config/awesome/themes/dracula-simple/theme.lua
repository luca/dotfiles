---------------------------
--  awesome theme --
---------------------------
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")

local math, string, os = math, string, os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local gfs = require("gears.filesystem")
local themes_path = os.getenv("HOME") .. "/.config/awesome/themes/dracula-simple"

-- Tags 
-- awful.util.tagnames =  { " 1 ", " 2" , " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { " dev ", " www ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ","  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { " I ", " II ", " III ", " IV ", " V ", " VI " }
-- awful.util.tagnames =  { "  ",  " ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { " 一 ", " 二 ", " 三 ", " 四 ", " 五 ", " 六 ", " 七 ", " 八 ", " 九 " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "" }
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
awful.util.tagnames = {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "}
-- awful.util.tagnames =  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }
-- awful.util.tagnames = {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "}

-- Colors.
local colors = {}
-- Dark
colors.black 									= "#21222c"
colors.red 										= "#ff5555"
colors.green 									= "#50fa7b"
colors.yellow 									= "#f1fa8c"
colors.blue 									= "#bd93f9"
colors.magenta 									= "#923C6D"
colors.cyan 									= "#8be9fd"
colors.white 									= "#f8f8f2"
-- Bright
colors.brightblack 								= "#44475a"
colors.brightred 								= "#ff6e6e"
colors.brightgreen 								= "#69ff94"
colors.brightyellow 							= "#ffffa5"
colors.brightblue 								= "#d6acff"
colors.brightmagenta 							= "#ff92df"
colors.brightcyan 								= "#a4ffff"
colors.brightwhite 								= "#ffffff"

local theme = {}

theme.font 										= "JetBrains Mono Bold 10"
theme.taglist_font 								= "Font Awesome 6 Free Solid 11"

theme.bg_normal 								= colors.black
theme.bg_focus  								= colors.black
theme.bg_urgent     							= colors.black
theme.bg_minimize   							= colors.brightblack
theme.bg_systray    							= colors.black

theme.taglist_bg_focus                          = colors.black
theme.taglist_fg_focus                          = colors.magenta
theme.taglist_fg_empty                          = colors.brightblack
theme.taglist_bg_empty                          = colors.black
theme.taglist_fg_urgent							= colors.green

theme.fg_normal     							= colors.white
theme.fg_focus      							= colors.magenta
theme.fg_urgent     							= colors.brightred
theme.fg_minimize   							= colors.white

theme.useless_gap   							= 4
theme.border_width  							= 2
theme.border_normal                             = colors.brightblack
theme.border_focus                              = colors.magenta
theme.border_marked                             = colors.magenta

theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = theme.fg_focus

theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_icon                     = false
-- There are other variable sets
-- overriding the  one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
-- local taglist_square_size = dpi(4)
-- theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
--     taglist_square_size, theme.fg_normal
-- )
-- theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
--     taglist_square_size, theme.fg_normal
-- )

-- Variables set for theming notifications:
notification_font								= theme.font
notification_bg									= colors.black
notification_fg									= colors.magenta
notification_border_color						= colors.magenta
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon 						= themes_path.."/icons/submenu.png"
theme.menu_height 								= 20
theme.menu_width  								= 180
theme.icon_theme                				= '/usr/share/icons/Papirus-Dark/16x16/apps'

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal 				= themes_path.."/titlebar/close_normal.png"
theme.titlebar_close_button_focus  				= themes_path.."/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal 			= themes_path.."/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  			= themes_path.."/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive 	= themes_path.."/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  	= themes_path.."/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active 		= themes_path.."/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  		= themes_path.."/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive 	= themes_path.."/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  	= themes_path.."/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active 		= themes_path.."/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  		= themes_path.."/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive 	= themes_path.."/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  	= themes_path.."/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active 	= themes_path.."/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  	= themes_path.."/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active 	= themes_path.."/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  	= themes_path.."/titlebar/maximized_focus_active.png"

theme.wallpaper 								= themes_path.."/background.png"
theme.awesome_icon  							= themes_path .."/icons/awesome.png"

-- You can use your own layout icons like this:
theme.layout_fairh 								= themes_path.."/layouts/fairh.png"
theme.layout_fairv 								= themes_path.."/layouts/fairv.png"
theme.layout_floating  							= themes_path.."/layouts/floating.png"
theme.layout_magnifier 							= themes_path.."/layouts/magnifier.png"
theme.layout_max 								= themes_path.."/layouts/max.png"
theme.layout_fullscreen 						= themes_path.."/layouts/fullscreen.png"
theme.layout_tilebottom 						= themes_path.."/layouts/tilebottom.png"
theme.layout_tileleft   						= themes_path.."/layouts/tileleft.png"
theme.layout_tile 								= themes_path.."/layouts/tile.png"
theme.layout_tiletop 							= themes_path.."/layouts/tiletop.png"
theme.layout_spiral  							= themes_path.."/layouts/spiral.png"
theme.layout_dwindle  							= themes_path.."/layouts/dwindle.png"
theme.layout_cornernw 							= themes_path.."/layouts/cornernww.png"
theme.layout_cornerne 							= themes_path.."/layouts/cornerne.png"
theme.layout_cornersw 							= themes_path.."/layouts/cornersw.png"
theme.layout_cornerse 							= themes_path.."/layouts/cornerse.png"


theme.widget_ac                                 = themes_path.."/icons/ac.png"
theme.widget_battery                            = themes_path.."/icons/battery.png"
theme.widget_battery_low                        = themes_path.."/icons/battery_low.png"
theme.widget_battery_empty                      = themes_path.."/icons/battery_empty.png"
theme.widget_mem                                = themes_path.."/icons/mem.png"
theme.widget_cpu                                = themes_path.."/icons/cpu.png"
theme.widget_temp                               = themes_path.."/icons/temp.png"
theme.widget_net                                = themes_path.."/icons/net.png"
theme.widget_hdd                                = themes_path.."/icons/hdd.png"
theme.widget_keybordicon                        = themes_path.."/icons/keybord.png"
theme.widget_music                              = themes_path.."/icons/note.png"
theme.widget_music_on                           = themes_path.."/icons/note.png"
theme.widget_music_pause                        = themes_path.."/icons/pause.png"
theme.widget_music_stop                         = themes_path.."/icons/stop.png"
theme.widget_vol                                = themes_path.."/icons/vol.png"
theme.widget_vol_low                            = themes_path.."/icons/vol_low.png"
theme.widget_vol_no                             = themes_path.."/icons/vol_no.png"
theme.widget_vol_mute                           = themes_path.."/icons/vol_mute.png"
theme.widget_mail                               = themes_path.."/icons/mail.png"
theme.widget_mail_on                            = themes_path.."/icons/mail_on.png"
theme.widget_task                               = themes_path.."/icons/task.png"
theme.widget_scissors                           = themes_path.."/icons/scissors.png"
theme.widget_weather                            = themes_path.."/icons/dish.png"

local markup = lain.util.markup
local separators = lain.util.separators
-- Generate Awesome icon:
--theme.awesome_icon = theme_assets.awesome_icon(
--    theme.menu_height, theme.bg_focus, theme.fg_focus
--)
-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({ }, 1, function(t) t:view_only() end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
		  client.focus:move_to_tag(t)
		end
	end),
	awful.button({ }, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end)
	--awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
	--awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
	awful.button({ }, 1, function (c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal(
				"request::activate",
				"tasklist",
				{raise = true}
			)
		end
	end),
	awful.button({ }, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
		end),
	awful.button({ }, 4, function ()
		awful.client.focus.byidx(1)
		end),
	awful.button({ }, 5, function ()
		awful.client.focus.byidx(-1)
end))
                                          
-- Keyboard map indicator and switcher
-- keyboardlayout = awful.widget.keyboardlayout()
local keybordicon = wibox.widget.imagebox(theme.widget_keybordicon)
keyboardlayout = awful.widget.keyboardlayout()
keyboardlayout.widget.font = theme.font
mykeyboardlayout = wibox.widget {
	{
		{
			widget = keyboardlayout
		},
        fg = colors.brightblue, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.brightblue,
	widget = wibox.container.margin
    
}
mykeyboardico = wibox.widget {
	{
		{
			widget = wibox.widget.textbox(" ")
		},
        fg = colors.brightblue, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.brightblue,
	widget = wibox.container.margin
    
}
keyboardlayout.widget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.widget.keyboardlayout() end)
))

-- {{{ Wibar
-- Create a textclock widget
-- mytextclock = wibox.widget.textclock('%a %d %b %H:%M ')
-- Textclock
local clock = awful.widget.watch(
    "date +'%a %d %b %H:%M' ", 60,
    function(widget, stdout)
        widget:set_markup(markup.fontfg(theme.font, colors.brightmagenta, "⧗ " .. stdout))
    end
)
myclock = wibox.widget {
	{
		{
          widget = clock
        },
        fg = colors.brightmagenta, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.brightmagenta,
	widget = wibox.container.margin
      
}

-- Calendar
theme.cal = lain.widget.cal({
    attach_to = { clock },
    notification_preset = {
        font = theme.font,
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

-- MEM
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, colors.green, " " .. mem_now.perc .. "% "))
    end
})
mymem = wibox.widget {
	{
        {
          widget = mem.widget
        },
        fg = colors.green, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.green,
	widget = wibox.container.margin
}

-- CPU
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, colors.blue, " " .. cpu_now.usage .. "% "))
    end
})
mycpu = wibox.widget {
	{
        {
          widget = cpu.widget
        },
        fg = colors.blue, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.blue,
	widget = wibox.container.margin
}


-- Coretemp (lain, average)
local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp = lain.widget.temp({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, colors.brightred," " .. coretemp_now .. "°C "))
    end
})
mytemp = wibox.widget {
	{
        {
          widget = temp.widget
        },
        fg = colors.brightred, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.brightred,
	widget = wibox.container.margin
}


--]]
-- Battery
local baticon = wibox.widget.imagebox(theme.widget_battery)
local bat = lain.widget.bat({
    settings = function()
        if bat_now.status and bat_now.status ~= "N/A" then
            if bat_now.ac_status == 1 then
                widget:set_markup(markup.fontfg(theme.font, colors.brightred, "  AC "))
                baticon:set_image(theme.widget_ac)
                return
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
                baticon:set_image(theme.widget_battery_empty)
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 15 then
                baticon:set_image(theme.widget_battery_low)
            else
                baticon:set_image(theme.widget_battery)
            end
            widget:set_markup(markup.fontfg(theme.font, colors.brightred, " " .. bat_now.perc .. "% "))
        else
            widget:set_markup()
            baticon:set_image(theme.widget_ac)
        end
    end
})
mybat = wibox.widget {
	{
        {
          widget = bat.widget
        },
        fg = colors.brightred, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.brightred,
	widget = wibox.container.margin
}

-- Net
local neticon = wibox.widget.imagebox(theme.widget_net)
local net = lain.widget.net({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, colors.yellow, "  " .. net_now.received .. " ↓↑ " .. net_now.sent .. " "))
    end
})
mynet = wibox.widget {
	{
        {
          widget = net.widget
        },
        fg = colors.yellow, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.yellow,
	widget = wibox.container.margin
}

-- Systemtry : 
mytry = wibox.widget {
	{
        {
          widget = wibox.widget.systray()
        },
        fg = colors.brightgreen, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	bottom = 2,
	color = colors.green,
	widget = wibox.container.margin
}

-- Separators
separt = wibox.widget.textbox("  ")

-- Tags 
-- awful.util.tagnames =  { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "dev", "www", "sys", "doc", "vbox", "chat", "mus", "vid", "gfx" }
-- awful.util.tagnames =  {"", "", "", "","", "", "", "", "", "", ""}
-- awful.util.tagnames =  { "I", "II", "III", "IV", "V", "VI" }
-- awful.util.tagnames =  {"", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  {"", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames =  { "一", "二", "三", "四", "五", "六", "七", "八", "九" }
-- awful.util.tagnames =  {"", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames =  {"", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames = {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "}
awful.util.tagnames = {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "}

function theme.at_screen_connect(s)
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom", screen = s, height = 24})

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            
            -- Net :
            mynet,
            separt,
            -- CPU TEMP :
            mytemp,
            separt,
            -- CPU :
            mycpu,
            separt, 
            -- RAM : 
            mymem, 
            separt,
            -- Battery :
            mybat,
            separt,
            -- Keybord :
            mykeyboardico,
            mykeyboardlayout,
            separt,
            -- Clock :
            myclock,
            separt,
            -- Sys Try :
            mytry,
            separt,
            -- Layouts :	
            s.mylayoutbox
        },
    }
end

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
return theme
